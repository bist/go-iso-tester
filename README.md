# Virtual Machine Creation and ISO Attachment Script

This script automates the creation of multiple Virtual Machines (VMs) using VirtualBox and ISO files located in a specified folder. It performs the following actions:
1. Detects ISO files in a folder.
2. Creates a virtual machine for each ISO.
3. Attaches a 50GB virtual hard disk and the ISO as a DVD drive.
4. Configures the VM to use a NAT network adapter.
5. Starts the VM in headless mode and sends keyboard input to boot from the ISO.

## Prerequisites

- **VirtualBox** must be installed.
- `VBoxManage` should be in the system path.
- Go environment is properly set up.

## Installation

1. Clone the repository or copy the script.
2. Ensure that the `isoDir` variable in the code points to the correct folder containing ISO files.
3. Compile the Go code:
    ```
    go build -o vm_creator
    ```

## Usage

Run the compiled executable:
```
./vm_creator
```


The script will:
- Automatically detect all `.iso` files in the provided directory.
- Create and configure VMs based on those ISOs.
- Boot the VMs and send keystrokes to boot from the ISO.

## Key Functions

### `getISOFiles(isoDir string) []string`
Retrieves a list of all ISO files in the specified directory.

### `createDisk(vmName string)`
Creates a 50GB virtual hard disk for the VM.

### `setupVM(vmName string) *virtualbox.Machine`
Creates a new VirtualBox machine with 4GB of memory.

### `setupStorageAndISO(vm *virtualbox.Machine, vmName string, isoPath string)`
Adds a SATA controller, attaches the virtual hard disk and the ISO file as a DVD drive.

### `configureVMAndStart(vm *virtualbox.Machine, vmName string)`
Configures the VM’s network adapter in bridged mode and starts the VM in headless mode.

### `waitAndSendKeystrokes(vmName string)`
Waits for the VM to boot and sends two down-arrow keystrokes followed by an "Enter" key press to boot from the attached ISO.

## Customization

You can modify the following parameters in the script:
- `isoDir`: Directory containing the ISO files.
- `vmName`: Template for the VM names.
- Memory size (`2048` MB) and disk size (`50GB`) can be adjusted as needed in the relevant functions.


