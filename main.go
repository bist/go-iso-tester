package main

import (
	"fmt"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"time"

	"github.com/terra-farm/go-virtualbox"
)

func main() {
	isoDir := "/home/bist/artix/isos" // Directory with ISOs
	isoFiles := getISOFiles(isoDir)

	// Loop through ISO files and create a VM for each
	for i, isoPath := range isoFiles {
		vmName := fmt.Sprintf("test-vm-%d", i+1)
		fmt.Printf("Creating VM: %s with ISO: %s\n", vmName, isoPath)

		// Step 1: Create a 50GB virtual disk
		createDisk(vmName)

		// Step 2: Create the VM and set memory size
		vm := setupVM(vmName)

		// Step 3: Add storage and attach ISO
		setupStorageAndISO(vm, vmName, isoPath)

		// Step 4: Configure network, save VM, and start it
		configureVMAndStart(vm, vmName)

		// Step 5: Boot and send keystrokes
		waitAndSendKeystrokes(vmName)

		fmt.Printf("VM %s created and started successfully\n", vmName)
	}
}

// Step 1: Create a 50GB virtual disk
func createDisk(vmName string) {
	cmdhd := exec.Command("VBoxManage", "createhd", "--filename", fmt.Sprintf("%s-disk.vdi", vmName), "--size", "51200") // 50GB
	if err := cmdhd.Run(); err != nil {
		log.Fatalf("Failed to create virtual disk: %v", err)
	}
	log.Println("50GB virtual disk created successfully")
}

// Step 2: Create the VM and set memory size
func setupVM(vmName string) *virtualbox.Machine {
	vm, err := virtualbox.CreateMachine(vmName, "Linux_64")
	if err != nil {
		log.Fatalf("Failed to create VM: %v", err)
	}
	cmdmem := exec.Command("VBoxManage", "modifyvm", vmName, "--memory", "2048")
	if err := cmdmem.Run(); err != nil {
		log.Fatalf("Failed to set memory size: %v", err)
	}
	log.Println("Memory size set to 2GB successfully")
	return vm
}

// Step 3: Add storage controllers and attach ISO
func setupStorageAndISO(vm *virtualbox.Machine, vmName string, isoPath string) {
	err := vm.AddStorageCtl("SATAController", virtualbox.StorageController{SysBus: virtualbox.SysBusSATA})
	if err != nil {
		log.Fatalf("Failed to add SATA controller: %v", err)
	}
	err = vm.AttachStorage("SATAController", virtualbox.StorageMedium{
		Port:      0,
		Device:    0,
		Medium:    fmt.Sprintf("%s-disk.vdi", vmName),
		DriveType: virtualbox.DriveHDD,
	})
	if err != nil {
		log.Fatalf("Failed to attach disk: %v", err)
	}
	cmdIDE := exec.Command("VBoxManage", "storagectl", vmName, "--name", "IDE", "--add", "ide")
	if err := cmdIDE.Run(); err != nil {
		log.Fatalf("Failed to add IDE controller: %v", err)
	}
	cmdiso := exec.Command("VBoxManage", "storageattach", vmName, "--storagectl", "IDE", "--port", "0", "--device", "0", "--type", "dvddrive", "--medium", isoPath)
	if err := cmdiso.Run(); err != nil {
		log.Fatalf("Failed to attach ISO file: %v", err)
	}
	log.Println("ISO file attached successfully")
}

// Step 4: Configure network and start the VM
func configureVMAndStart(vm *virtualbox.Machine, vmName string) {
	cmdnet := exec.Command("VBoxManage", "modifyvm", vmName, "--nic1", "NAT")
	if err := cmdnet.Run(); err != nil {
		log.Fatalf("Failed to add network adapter: %v", err)
	}
	err := vm.Save()
	if err != nil {
		log.Fatalf("Failed to save VM: %v", err)
	}
	cmdstart := exec.Command("VBoxManage", "startvm", vmName, "--type", "headless")
	if err := cmdstart.Run(); err != nil {
		log.Fatalf("Failed to start VM: %v", err)
	}
	log.Println("VM started successfully")
}

// Step 5: Boot VM and send keystrokes
func waitAndSendKeystrokes(vmName string) {
	sleepDuration := 20
	for i := sleepDuration; i > 0; i-- {
		fmt.Printf("Waiting for VM to boot... %d seconds remaining\n", i)
		time.Sleep(1 * time.Second)
	}
	for i := 0; i < 2; i++ {
		exec.Command("VBoxManage", "controlvm", vmName, "keyboardputscancode", "50").Run()
		exec.Command("VBoxManage", "controlvm", vmName, "keyboardputscancode", "d0").Run()
		time.Sleep(1 * time.Second)
	}
	exec.Command("VBoxManage", "controlvm", vmName, "keyboardputscancode", "1c").Run()
	exec.Command("VBoxManage", "controlvm", vmName, "keyboardputscancode", "9c").Run()
	log.Println("Keystrokes sent successfully")
}

// Function to retrieve ISO files from a folder
func getISOFiles(isoDir string) []string {
	files, err := os.ReadDir(isoDir)
	if err != nil {
		log.Fatalf("Failed to read ISO directory: %v", err)
	}

	var isoFiles []string
	for _, file := range files {
		if !file.IsDir() && strings.HasSuffix(file.Name(), ".iso") {
			isoFiles = append(isoFiles, filepath.Join(isoDir, file.Name()))
		}
	}
	return isoFiles
}
